<?php


Route::get('/', function () {
    return view('welcome');
});


Route::get('online/users', function () {
    return view('users');
});

Route::post('/user-online', 'HomeController@newUserOnline');

Auth::routes();

Route::get('/home', 'HomeController@index');
