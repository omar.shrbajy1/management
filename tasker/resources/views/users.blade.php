<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="user-card-home-page">
    <div class="user-card--home-page-header"></div>
    <div class="user-card-home-page-body">
        <img class="user-card-home-page-img center-block" src="http://placehold.it/70x70">
    </div>
    <div class="user-card-home-page-footer text-center">
        <h5 class="">Hisham Jihad Shikha</h5>
    </div>
</div>
</body>
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.8/socket.io.js"></script>

<script>
    var socket = io('http://localhost:3000');

    socket.on('fire', function (message) {
        console.log(message)
        $('body').html('');
        for (var i = 0; i < message.length; i++) {
            $('body').append('<div class="user-card-home-page">' +
                ' <div class="user-card--home-page-header"></div>' +
                '   <div class="user-card-home-page-body">' +
                '   <img class="user-card-home-page-img center-block" src="http://placehold.it/70x70">' +
                '   </div>' +
                '   <div class="user-card-home-page-footer text-center">' +
                '      <h5 class="">' + message[i].name + '</h5>' +
                '  </div>' +
                ' </div>')
        }
    })
    socket.on('connect', function () {
        console.log(socket.io.engine.id)
    })


</script>
</html>