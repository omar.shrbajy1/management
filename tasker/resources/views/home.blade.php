@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    {{csrf_field()}}
                    <div class="panel-body">
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.8/socket.io.js"></script>
    <script>

        var socket = io('http://localhost:3000');

        socket.on('fire', function (message) {
            console.log(message)
        })
        socket.on('connect', function () {
            $.ajax('/user-online', {
                type: 'post',
                data: {socket_id: socket.io.engine.id, '_token': $('input').val()}
            });

            console.log(socket.io.engine.id)
        })


    </script>
@endsection
