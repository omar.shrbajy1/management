module.exports = {

    users: [],

    userIsOnline: function (user) {
        var b = 0;
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].id !== user.id) {
                b = 1;
                break;
            }
        }
        // this.users.push(user);
        if (b === 1 || b === 0 && this.users.length === 0) {
            this.users.push(user);
        }

    },
    userIsOffline: function (socket_id) {
        this.users = this.users.filter(function(item) {
            return item.socket_id !== socket_id
        });
    }

}