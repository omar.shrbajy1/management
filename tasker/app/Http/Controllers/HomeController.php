<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function newUserOnline(Request $request)
    {
        $data =  ['id' => Auth::id(), 'name' => Auth::user()->name, 'socket_id' => $request['socket_id']] ;
        Redis::publish('test-channel', json_encode($data));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
