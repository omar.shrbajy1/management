var server = require('http').Server();
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis();
var user = require('./users');


redis.subscribe('test-channel');

redis.on('message', function (channel, message) {
    socket_id = '\/#' + JSON.parse(message).socket_id;
    console.log(socket_id)
    user.userIsOnline(JSON.parse(message))
    io.emit('fire', user.users);

});


io.on('connection', function (socket) {


    console.log('connected')
    // console.log('connected');
    // console.log(((socket.id).substring(2)))
    // console.log('______________________________________________________________');


    socket.on('disconnect', function () {
        user.userIsOffline((socket.id).substring(2));
        io.emit('fire', user.users);
        console.log(user.users);
        console.log('Got disconnect!');
        console.log(socket.id)
        console.log('__________________________');

    });
});

server.listen(3000, function () {
    console.log('listen on port 3000');
});
